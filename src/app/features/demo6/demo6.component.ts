import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { BehaviorSubject, Observable, of, Subject } from 'rxjs';
import { combineLatest } from 'rxjs';
import { AuthService } from '../../core/auth.service';
import { ThemeService } from '../../core/theme.service';

@Component({
  selector: 'app-demo6',
  template: `
    <p>
      demo6 works! {{themeSrv.theme$ | async}}
    </p>

    <input type="text" [formControl]="input">

    <button (click)="authSrv.login()">login simulation</button>
  `,
  styles: [
  ]
})
export class Demo6Component {
  input = new FormControl()
  constructor(
    public themeSrv: ThemeService,
    public authSrv: AuthService
  ) {

    combineLatest([
      this.input.valueChanges,
      this.themeSrv.theme$
    ])
      .subscribe(val => console.log(val))
  }

  doSomething(value: string) {
  }
}
