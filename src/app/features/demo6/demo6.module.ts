import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { Demo6RoutingModule } from './demo6-routing.module';
import { Demo6Component } from './demo6.component';


@NgModule({
  declarations: [
    Demo6Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Demo6RoutingModule
  ]
})
export class Demo6Module { }
