import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  catchError, concatMap,
  debounce,
  debounceTime, delay, exhaustMap,
  forkJoin,
  interval,
  map,
  mergeAll,
  mergeMap,
  of, skipWhile,
  switchMap,
  take,
  tap
} from 'rxjs';

@Component({
  selector: 'app-demo2',
  template: `
    <p>
      demo2 works!
    </p>
    <input type="text" [formControl]="input">
    <!--<button routerLink="../61">next</button>-->
    <button  (click)="gotonext()">next</button>
  `,
  styles: [
  ]
})
export class Demo2Component {
  input = new FormControl('', { nonNullable: true })

  index = 1;

  gotonext() {
    ++this.index;
    this.router.navigateByUrl('/demo2/' + this.index.toString())
  }

  users$ = (text: string) => this.http.get<User[]>('https://jsonplaceholder.typicode.com/users?q=' + text)
    .pipe(
      map(res => res.filter(user => user.id > 5)),
      catchError(err => {
        return of([])
      })
    )

  posts$ = this.http.get<any[]>('https://jsonplaceholder.typicode.com/posts');
  timer$ = interval(1000).pipe(take(2));


  constructor(
    private http: HttpClient,
    private activateRoute: ActivatedRoute,
    private router: Router
  ) {
    const getTodos = (userAndPost: any) => this.http.get<Todo[]>('https://jsonplaceholder.typicode.com/todos?userId=' + userAndPost.user.id)
      .pipe(
        map(todos => ({ todos, ...userAndPost }))
      )
    const getUser = (id: number) => this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id)

    // OUTER
    interval(1).pipe(take(10), skipWhile(count => count < 1))
      .pipe(
        // INNER
        exhaustMap(id => this.http.get<User>('https://jsonplaceholder.typicode.com/users/' + id).pipe()),
       /* mergeMap(post => getUser(post.userId)
          .pipe(
            map(user => ({ user, post }))
          )
        ),
        mergeMap(userAndPost => getTodos(userAndPost) )*/
      )

      .subscribe({
        next: (res) => {
          console.log('result', res)
        },
        error: (err) => console.log('err', err),
        complete: () => console.log('completed')
      })
  }
}

interface User {
  id: number,
  name: string;
}

interface Post {
  id: number,
  userId: number,
  title: string;
}

interface Todo {
  id: number,
  userId: number,
  title: string;
  completed: boolean;
}
