import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';

import { Demo2RoutingModule } from './demo2-routing.module';
import { Demo2Component } from './demo2.component';


@NgModule({
  declarations: [
    Demo2Component
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    Demo2RoutingModule
  ]
})
export class Demo2Module { }
