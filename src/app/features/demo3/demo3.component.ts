import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { concatMap, exhaustMap, interval, map, mergeMap, switchMap, take } from 'rxjs';

@Component({
  selector: 'app-demo3',
  template: `
    <p>
      demo3 works!
    </p>
    <input type="number" [formControl]="input">
  `,
  styles: [
  ]
})
export class Demo3Component {
  input = new FormControl(0, { nonNullable: true })

  constructor() {
    this.input.valueChanges
      .pipe(
        exhaustMap(value => interval(1000).pipe(
          take(3),
          map(() => value * 10)
        ))
      )
      .subscribe(console.log)
  }
}
