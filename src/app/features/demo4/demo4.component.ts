import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { FormControl } from '@angular/forms';
import { combineLatest, forkJoin, interval, mergeMap } from 'rxjs';

@Component({
  selector: 'app-demo4',
  template: `
    <p>
      demo4 works!
    </p>
    <input type="text" [formControl]="input">
    <input type="text" [formControl]="input2">

    <li *ngFor="let user of data$ | async">
      {{user.name}}
    </li>
    
  `,
  styles: [
  ]
})
export class Demo4Component {
  input = new FormControl('', { nonNullable: true })
  input2 = new FormControl('', { nonNullable: true })

  data$ = combineLatest({
    text1: this.input.valueChanges,
    text2: this.input2.valueChanges,
  })
    .pipe(
      mergeMap(obj => this.http.get<any[]>('https://jsonplaceholder.typicode.com/users/xxx?q=' + obj.text1))
    )

  constructor(private http: HttpClient) {

  }
}
