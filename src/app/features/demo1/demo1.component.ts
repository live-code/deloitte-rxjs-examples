import { logMessages } from '@angular-devkit/build-angular/src/builders/browser-esbuild/esbuild';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, ElementRef, ViewChild } from '@angular/core';
import {
  buffer, catchError, delay,
  filter,
  fromEvent,
  interval,
  map,
  of,
  reduce, retry,
  skipUntil,
  Subscription,
  take,
  takeUntil,
  takeWhile,
  tap
} from 'rxjs';
import { ajax } from 'rxjs/ajax';

@Component({
  selector: 'app-demo1',
  template: `
    <p>
      demo1 works!
      <button #btn>CLICK </button>
    </p>
  `,
})
export class Demo1Component {
  @ViewChild('btn') button!: ElementRef<HTMLButtonElement>

  constructor(private http: HttpClient) {
  }
  ngAfterViewInit() {
    this.http.get<User>('https://jsonplaceholder.typicode.com/usersss')
      .subscribe({
        next: (val) => {
          console.log('val', val)
        },
        error: (err) => console.log('err', err),
        complete: () => console.log('completed')
      })


  }
}


interface User {
  id: number,
  name: string;
}
