import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { Demo5RoutingModule } from './demo5-routing.module';
import { Demo5Component } from './demo5.component';


@NgModule({
  declarations: [
    Demo5Component
  ],
  imports: [
    CommonModule,
    Demo5RoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class Demo5Module { }
