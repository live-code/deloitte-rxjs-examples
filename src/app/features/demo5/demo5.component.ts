import { HttpClient } from '@angular/common/http';
import { Component } from '@angular/core';
import { concatMap, filter, map, mergeMap, of, Subscription, switchMap, tap, toArray } from 'rxjs';

const API = 'https://jsonplaceholder.typicode.com';


@Component({
  selector: 'app-demo5',
  template: `
    <p>
      demo5 works!
    </p>
    
    <div *ngFor="let item of users">
      <h1>{{item.user.name}}</h1>
      <div *ngFor="let todo of item.todos">
        <input type="checkbox" [ngModel]="todo.completed">
        {{todo.title}}
      </div>
    </div>
  `,
  styles: [
  ]
})
export class Demo5Component {
  users: { user: User, todos: Todo[]}[] = []
  sub!: Subscription;

  constructor(private http: HttpClient) {
    this.sub = this.http
      .get<User[]>(`${API}/users/`)
      .pipe(
        mergeMap(users => users),
        concatMap(
          user => this.http.get<Todo[]>(`${API}/todos/?userId=${user.id}`)
            .pipe(
              map(todos => ({ user, todos }))
            )
        ),
      )
      .subscribe(user => {
        this.users.push(user)
      })
  }

  ngOnDestroy(){
    this.sub.unsubscribe();
  }
}



export interface Todo {
  id: number;
  userId: number;
  title: string;
  completed: false;
}
​
export interface User {
  id: number;
  name: string;
  username: string;
  email: string;
}
