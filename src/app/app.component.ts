import { Component } from '@angular/core';
import { AuthService } from './core/auth.service';
import { ThemeService } from './core/theme.service';

@Component({
  selector: 'app-root',
  template: `
    <button routerLink="demo1">demo1</button>
    <button routerLink="demo2">demo2</button>
    <button routerLink="demo3">demo3</button>
    <button routerLink="demo4">demo4</button>
    <button routerLink="demo5">demo5</button>
    <button routerLink="demo6">demo6</button>

    {{(authService.auth$ | async)?.role}}
    <button (click)="themeService.changeTheme('it')">It</button>
    <button (click)="themeService.changeTheme('en')">En</button>
    <button (click)="authService.logout()">quit</button>
    <router-outlet></router-outlet>
  `,
})
export class AppComponent {

  constructor(
    public themeService: ThemeService,
    public authService: AuthService,
  ) {
  }
}
