import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'demo1', loadChildren: () => import('./features/demo1/demo1.module').then(m => m.Demo1Module) },
  { path: 'demo2', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'demo2/:id', loadChildren: () => import('./features/demo2/demo2.module').then(m => m.Demo2Module) },
  { path: 'demo3', loadChildren: () => import('./features/demo3/demo3.module').then(m => m.Demo3Module) },
  { path: 'demo4', loadChildren: () => import('./features/demo4/demo4.module').then(m => m.Demo4Module) },
  { path: 'demo5', loadChildren: () => import('./features/demo5/demo5.module').then(m => m.Demo5Module) },
  { path: 'demo6', loadChildren: () => import('./features/demo6/demo6.module').then(m => m.Demo6Module) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
