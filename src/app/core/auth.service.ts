import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  auth$ = new BehaviorSubject<Auth | null>(null);

  login() {
    // http...
    const res: Auth = { token: 'wefwe', role: 'admin'}
    this.auth$.next(res)
  }

  logout() {
    this.auth$.next(null)
  }

  isLogged() {
    // todo
  }

  getRole() {
    // todo
  }
}


interface Auth{
  token: string;
  role: string;
}
