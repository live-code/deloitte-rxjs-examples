import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { catchError, delay, Observable, of, retry, throwError } from 'rxjs';
import { environment } from '../../environments/environment';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor() {}

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request)
      .pipe(
        retry({ count: 1, delay: 500 }),
        catchError(err => {
          switch(err.status) {
            case 401:
              // redirect to login
              break;
          }
          return throwError(err)
        }),
        delay(500)
        // delay(environment.production ? 0 : 1000)
      )
  }
}
