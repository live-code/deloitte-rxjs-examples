import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ThemeService {
  theme$ = new BehaviorSubject('it');

  constructor() { }

  changeTheme(value: string) {
    this.theme$.next(value)
  }
}
